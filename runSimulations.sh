#!/bin/bash

AmountOfIterations=7

declare -a Reproduction_minimum=('90' '125' '160');
declare -a Newborn_energy=('50' '100' '150');
declare -a Transferred_energy=('20' '40' '60');
AmountOfTries=26

#for j in `seq 0 $AmountOfTries`;
#do
#	RM=${Reproduction_minimum[$(($j%3))]}
#	NE=${Newborn_energy[$((($j/3)%3))]}
#	TE=${Transferred_energy[$((($j/9)%3))]}
	RM=90
	NE=100
	TE=40

	ITS=20
	BT=50
	MHL=1.2
	IM=5
	GAE=100	

	j=2

	mkdir ../simulation_$j
	mkdir ../simulation_$j/emas
	mkdir ../simulation_$j/emas/logs
	mkdir ../simulation_$j/iemas
	mkdir ../simulation_$j/iemas/logs

	touch ../simulation_$j/parameters
	echo -e 'reproduction_minimum: '$RM'\nnewborn_energy: '$NE'\ntransferred_energy: '$TE'\nimmunological_time_span: '$ITS'\nbite_transfer: '$BT'\nmahalanobis: '$MHL'\nimmunological_maturity: '$IM'\ngood_agent_energy: '$GAE > ../simulation_$j/parameters

	mkdir Output
	for i in `seq 0 $AmountOfIterations`;
	do
		python -m pyage.core.bootstrap femas_single_iemas 1 0 $RM 9999 $NE $TE $ITS $BT $MHL $IM $GAE
		FILE1=$(find . -maxdepth 1 -name "*.txt")
		mv $FILE1 Output/sim_$i
	done

	python prepareDataToGnuplot.py $AmountOfIterations > data_gnuplot_iemas

	mv *.log ../simulation_$j/iemas/logs
	mv Output ../simulation_$j/iemas/


	mkdir Output
	for i in `seq 1 $AmountOfIterations`;
	do
		python -m pyage.core.bootstrap femas_single_emas 1 0 $RM 9999 $NE $TE
		FILE2=$(find . -maxdepth 1 -name "*.txt")
		mv $FILE2 Output/sim_$i
	done

	python prepareDataToGnuplot.py $AmountOfIterations > data_gnuplot_emas
	gnuplot draw.gnuplot

	mv *.log ../simulation_$j/emas/logs
	mv Output ../simulation_$j/emas/

	mv data_gnuplot_emas ../simulation_$j
	mv data_gnuplot_iemas ../simulation_$j
	mv *.png ../simulation_$j/
#done

rm *.pyc
