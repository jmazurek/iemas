# coding=utf-8
import logging
import os
import sys
import cProfile

from math import pi

from pyage.core import address
from pyage.core.migration import ParentMigration
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.solutions.evolution.crossover import SinglePointCrossover
from pyage.solutions.evolution.mutation import UniformFloatMutation

from pyage.core.agent.agent import unnamed_agents
from IEMAS.core.aggregate import AggregateAgent
from IEMAS.core.locator import GridParentLocator
from IEMAS.core.iemas import ImmunologicalEmasService
from IEMAS.core.statistics import AgentsStatistics
from IEMAS.solutions.evolution.evaluation import FloatRastriginEvaluation
from IEMAS.solutions.evolution.evaluation import DeJongEvaluation
from IEMAS.solutions.evolution.evaluation import SchwefelEvaluation
from IEMAS.solutions.evolution.evaluation import MichalewiczEvaluation
from IEMAS.solutions.evolution.evaluation import DeJongFloatEvaluation
from IEMAS.solutions.evolution.initializer import float_iemas_initializer

# parsing input args
if len(sys.argv) == 9:
    agents_count = int(sys.argv[2])
    minimal_energy = lambda: int(sys.argv[3])
    reproduction_minimum = lambda: int(sys.argv[4])
    migration_minimum = lambda: int(sys.argv[5])
    newborn_energy = lambda: int(sys.argv[6])
    transferred_energy = lambda: int(sys.argv[7])
	
    eval_temp = sys.argv[8]
    if eval_temp == "schwefel":
        evaluation = SchwefelEvaluation
        aggregated_agents = lambda: float_iemas_initializer(50, energy=100, size=100, lowerbound=-500, upperbound=500)
    elif eval_temp == "rastrigin":
        evaluation = FloatRastriginEvaluation
        aggregated_agents = lambda: float_iemas_initializer(50, energy=100, size=100, lowerbound=-5.12, upperbound=5.12)
    elif eval_temp == "michalewicz":
        evaluation = MichalewiczEvaluation
        aggregated_agents = lambda: float_iemas_initializer(50, energy=100, size=100, lowerbound=0, upperbound=pi)
    elif eval_temp == "dejong":
        evaluation = DeJongFloatEvaluation
        aggregated_agents = lambda: float_iemas_initializer(50, energy=100, size=100, lowerbound=-5.12, upperbound=5.12)
    else:
        evaluation = FloatRastriginEvaluation
        aggregated_agents = lambda: float_iemas_initializer(50, energy=100, size=100, lowerbound=-5.12, upperbound=5.12)
else:
    agents_count = int(os.environ['AGENTS'])
    minimal_energy = lambda: 0
    reproduction_minimum = lambda: 90
    migration_minimum = lambda: 9999
    newborn_energy = lambda: 100
    transferred_energy = lambda: 60
    evaluation = FloatRastriginEvaluation
    aggregated_agents = lambda: float_iemas_initializer(50, energy=100, size=100, lowerbound=-5.12, upperbound=5.12)
#end of parsing

logger = logging.getLogger(__name__)
logger.debug("EMAS, %s agents", agents_count)

agents = unnamed_agents(agents_count, AggregateAgent)

stop_condition = lambda: StepLimitStopCondition(1000)

emas = ImmunologicalEmasService

#immunological parameters
immunological_time_span = lambda: 0
bite_transfer = lambda: 0
mahalanobis = lambda: 1.4
immunological_maturity = lambda: 0
good_agent_energy = lambda: 100

crossover = SinglePointCrossover
mutation = lambda: UniformFloatMutation(probability=1, radius=1)
address_provider = address.SequenceAddressProvider
params = lambda: 'ME{0}RM{1}NE{2}ITS{3}BT{4}M{5}IM{6}GAE{7}'.format(minimal_energy(), reproduction_minimum(),
                                                                    newborn_energy(), immunological_time_span(),
                                                                    bite_transfer(), mahalanobis(),
                                                                    immunological_maturity(), good_agent_energy())
migration = ParentMigration
locator = GridParentLocator

stats = lambda: AgentsStatistics(aggregated_agents(), immunological_maturity())
