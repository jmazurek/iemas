import logging

from pyage.core.address import Addressable
from pyage.core.inject import Inject
from pyage.core.emas import EmasService

import numpy as np
import math

logger = logging.getLogger(__name__)


def assimility(g1, g2):
    return math.sqrt(sum([((x[0] - x[1]) * (x[0] - x[1])) for x in zip(g1, g2)]))


def MahalanobisDist(x, y):
    covariance_xy = np.cov(x, y, rowvar=0)
    inv_covariance_xy = np.linalg.inv(covariance_xy)
    xy_mean = np.mean(x), np.mean(y)
    x_diff = np.array([x_i - xy_mean[0] for x_i in x])
    y_diff = np.array([y_i - xy_mean[1] for y_i in y])
    diff_xy = np.transpose([x_diff, y_diff])

    md = 0
    for i in range(len(diff_xy)):
        md += np.sqrt(np.dot(np.dot(np.transpose(diff_xy[i]), inv_covariance_xy), diff_xy[i]))
    return (md / len(diff_xy))


class ImmunologicalEmasService(EmasService):
    @Inject("minimal_energy", "reproduction_minimum", "migration_minimum", "newborn_energy")
    def __init__(self):
        super(ImmunologicalEmasService, self).__init__()

    def should_die(self, agent):
        return agent.get_energy() <= self.minimal_energy

    def should_reproduce(self, a1, a2):
        return a1.get_energy() > self.reproduction_minimum and a2.get_energy() > self.reproduction_minimum

    def can_migrate(self, agent):
        return agent.get_energy() > self.migration_minimum and len(agent.parent.get_agents()) > 10

    def reproduce(self, a1, a2):
        logger.debug(str(a1) + " " + str(a2) + "reproducing!")
        becikowe = a1.parent.get_reservoir_energy_value()/10
        a1.parent.change_reservoir_energy(-becikowe)
        energy = self.newborn_energy / 2 * 2
        a1.energy -= self.newborn_energy / 2
        a2.add_energy(-self.newborn_energy / 2)
        genotype = a1.crossover.cross(a1.genotype, a2.get_genotype())
        a1.mutation.mutate(genotype)
        a1.parent.add_agent(VampireAgent(genotype, energy + becikowe))


class VampireAgent(Addressable):
    @Inject("locator", "migration", "evaluation", "crossover", "mutation", "emas", "transferred_energy",
            "immunological_time_span", "bite_transfer", "immunological_maturity", "good_agent_energy", "mahalanobis")
    def __init__(self, genotype, energy, name=None):
        self.name = name
        self.dead = False
        super(VampireAgent, self).__init__()
        self.genotype = genotype
        self.energy = energy
        self.steps = 0
        self.evaluation.process([genotype])
        self.bites = 0
        self.fitness_calls = 2

    def step(self):
        if self.fitness_calls > 0:
            self.fitness_calls -= 1
        self.steps += 1
        self.bites = 0
        try:
            neighbour = self.locator.get_neighbour(self)
            if neighbour and not neighbour.dead:
                if self.dead:
                    if self.steps < self.immunological_time_span:
                        self.haunt(neighbour)
                    else:
                        self.true_death()
                else:
                    if self.emas.should_die(self):
                        self.death()
                    elif self.emas.should_reproduce(self, neighbour):
                        self.emas.reproduce(self, neighbour)
                    else:
                        self.meet(neighbour)
                    if self.emas.can_migrate(self):
                        self.migration.migrate(self)
        except Exception as e:
            print e
            logging.exception('')

    def get_fitness(self):
        return self.genotype.fitness

    def get_best_genotype(self):
        return self.genotype

    def add_energy(self, energy):
        self.energy += energy

    def get_energy(self):
        return self.energy

    def get_genotype(self):
        return self.genotype

    def get_state(self):
        return self.dead()

    def meet(self, neighbour):
        logger.debug(str(self) + "meets" + str(neighbour))
        if self.get_fitness() > neighbour.get_fitness():
            transfered_energy = min(self.transferred_energy, neighbour.energy)
            self.energy += transfered_energy
            neighbour.add_energy(-transfered_energy)
        elif self.get_fitness() < neighbour.get_fitness():
            transfered_energy = min(self.transferred_energy, self.energy)
            self.energy -= transfered_energy
            neighbour.add_energy(transfered_energy)

    def death(self):
        self.distribute_energy()
        self.energy = 0
        self.dead = True
        logger.debug(str(self) + "died!")

    def distribute_energy(self):
        logger.debug("death, energy level: %d" % self.energy)
        if self.energy > 0:
            siblings = set(self.parent.get_agents())
            siblings.remove(self)
            portion = self.energy / len(siblings)
            if portion > 0:
                logger.debug("passing %d portion of energy to %d agents" % (portion, len(siblings)))
                for agent in siblings:
                    agent.add_energy(portion)
            left = self.energy % len(siblings)
            logger.debug("distributing %d left energy" % left)
            while left > 0:
                siblings.pop().add_energy(1)
                left -= 1

    def haunt(self, neighbour):
        logger.debug(str(self) + "haunts " + str(neighbour))
        if MahalanobisDist(self.get_genotype().genes, neighbour.get_genotype().genes) < self.mahalanobis:
            if self.steps < self.immunological_maturity:
                if neighbour.energy > self.good_agent_energy:
                    self.true_death()
            else:
                self.bites += 1
                transferred_energy = min(self.bite_transfer, neighbour.energy)
                self.parent.change_reservoir_energy(transferred_energy)
                neighbour.add_energy(-transferred_energy)

    def true_death(self):
        self.distribute_energy()
        self.energy = 0
        self.parent.remove_agent(self)
        logger.debug(str(self) + "died!")
